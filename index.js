console.log("Hi, A!");

// result: html html code
console.log(document);

// manipulation/targeting of/a specific id/portion in html
const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);

const txtLastName = document.querySelector("#txt-last-name");
console.log(txtLastName);



// alternatives
/*
	>> document.getElementById("txt-first-name");
	>> document.getElementByClassName("txt-class");
	>> document.getElementByTagName("h1");
*/

const fullName = document.querySelector("#span-full-name");
console.log(fullName);


// Event Listeners
/*
	keyup - once na nag up ung key or cursor mo dun palang magiinput ung tinype mo.
	(event)/(e) - is the event itself
	.innerHTML - all tags in html file has it in js file to dynamically change the value
*/
// Types of Events
/*
	change
	click
	load
	keydown
	keyup
*/

// Stretch version of (event) function

/*const keyCodeEvent = (e) => {
	let kc = e.keyCode;
	if(kc === 65){
		e.target.value = null;
		alert('Someone clicked a');
	}
}

txtFirstName.addEventListener('keyup', keyCodeEvent);*/



/*txtFirstName.addEventListener('keyup', (event) => {
	fullName.innerHTML = txtFirstName.value + '' + txtLastName.value ; 
})*/

/*txtFirstName.addEventListener('keyup', (event) => {
	// Knowing the event and it's value and behind processes
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
	// fullName.innerHTML = txtFirstName.value; //txtLastName.value;
})*/

// Activity 

const keyCodeEventFirstName = (e) => {
	fullName.innerHTML = (txtFirstName.value + ' ' + txtLastName.value); 
}

const keyCodeEventLastName = (e) => {
	fullName.innerHTML = (txtFirstName.value + ' ' + txtLastName.value); 
}


txtFirstName.addEventListener('keyup', keyCodeEventFirstName);
txtLastName.addEventListener('keyup', keyCodeEventLastName);
